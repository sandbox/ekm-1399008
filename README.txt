Description
-----------
Allows authentication to Drupal based on client side SSL certificates.

Requirements
----------
Apache 2.x
SSL Server certificates
SSL Client certificates

Install
----------
Enable SSL on the server for both the server (https) and the client (client
side SSL authentication). An example VirtualHost configuration is included
for this purpose with this archive in the apache.conf.txt file. 

Please note that you do not have to use the same CA/SSL chain for server
and client authentication.

Install this module as usual.

Automatic login and Automatically Create User on the admin page at
/admin/config/people/sslcert_auth if so desired.

Please note that enabling automatic login without automatic creation of login
names will create an error for those who provide an SSL certificate that do not
have a corresponding account on the server.

If you do not wish to enable automatic login, you can point the user to
/user/sslauth for authentication.

TODO:
* Add testing code on the admin page to return information about currently
returned fields from the current SSL certificate.
* Look at replacing the hook_auth_init implemented in sslcert_auth_init with
something a bit more elegant. 
